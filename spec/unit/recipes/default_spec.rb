# Cookbook:: template
# Spec:: default
#
# Copyright:: 2017, GitLab B.V., MIT.

require 'spec_helper'

describe 'gitlab_kafka::default' do
  context 'default execution' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.converge(described_recipe)
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'should include the java recipe' do
      expect(chef_run).to include_recipe('java')
    end
  end
end

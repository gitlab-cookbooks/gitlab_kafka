name             'gitlab_kafka'
maintainer       'GitLab Inc.'
maintainer_email 'ops-contact+cookbooks@gitlab.com'
license          'MIT'
description      'Cookbook template for Apache Kafka'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.0.1'
chef_version     '>= 12.1' if respond_to?(:chef_version)
issues_url       'https://gitlab.com/gitlab-cookbooks/gitlab_kafka/issues'
source_url       'https://gitlab.com/gitlab-cookbooks/gitlab_kafka'

depends 'java', '~> 1.50.0'
depends 'kafka', '~> 2.2.2'
depends 'lvm', '~> 4.1'
depends 'zookeeper', '~> 9.0.1'
supports 'ubuntu', '= 16.04'
